<?php
$I = new FunctionalTester($scenario);
$I->am('A moduleleader1');
$I->wantTo('Check that I can see a progress bar');

//Auththenication
Auth::loginUsingId(13);
$I->seeAuthentication();

//See landing page

$I->amOnPage('/admin/dash');

//see things on the landing page
$I->see('Add modules');
$I->see('Add an item');
$I->see('My Modules');
$I->see('All Items');

//Going on to module page
$I->click('My Modules');
$I->amOnPage('/admin/modules');
$I->see('testing one');
$I->see('testing two');
$I->see('testing three');
$I->seeElement('span', ['class' => 'meter']);
$I->see('62.5% Completed');
//Seeing individual progress, THEN
$I->click('testing one');
$I->amOnPage('admin/modules/11');
$I->seeElement('span', ['class' => 'meter']);
$I->see('62.5% Completed');
