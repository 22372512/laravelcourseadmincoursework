<?php
$I = new FunctionalTester($scenario);
$I->am('A moduleleader1');
$I->wantTo('Check that I can create a course');

//Auththenication
Auth::loginUsingId(13);
$I->seeAuthentication();

//See landing page

$I->amOnPage('/admin/dash');

//see things on the landing page
$I->see('Add modules');
$I->see('Add an item');
$I->see('My Modules');
$I->see('All Items');
//Creating a course
$I->click('Add modules');
$I->amOnPage('/admin/modules/create');
$I->fillField('title', 'Testing 2');
$I->fillField('code', '123456789');
$I->click('Create Module');
//then go to the module page
$I->amOnPage('/admin/modules/15');
$I->see('Testing 2');
