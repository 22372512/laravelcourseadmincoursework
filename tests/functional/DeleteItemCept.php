<?php
$I = new FunctionalTester($scenario);
$I->am('A moduleleader1');
$I->wantTo('Check that I can delete an item');

//Auththenication
Auth::loginUsingId(13);
$I->seeAuthentication();

//See landing page

$I->amOnPage('/admin/dash');

//see things on the landing page
$I->see('Add modules');
$I->see('Add an item');
$I->see('My Modules');
$I->see('All Items');

//Going on to module page
$I->click('My Modules');
$I->amOnPage('/admin/modules');
$I->see('testing one');
$I->click('testing one');
$I->amOnPage('/admin/modules/11');
$I->see('testing one');
$I->see('Add/Remove Item');
$I->see('Accusamus et libero minima delectus illum ut.');
$I->see('Accusantium est quis omnis aut in.');
$I->click('Add/Remove Item');
$I->amOnPage('/admin/modules/11/edit');
$I->uncheckOption('itemModules[19]');
$I->click('Update Module');
$I->amOnPage('/admin/modules/11');
$I->see('Accusamus et libero minima delectus illum ut.');
$I->dontSee('Accusantium est quis omnis aut in.');


