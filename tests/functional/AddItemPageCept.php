<?php
$I = new FunctionalTester($scenario);
$I->am('A moduleleader1');
$I->wantTo('Check that I can add an item');

//Auththenication
Auth::loginUsingId(13);
$I->seeAuthentication();

//See landing page

$I->amOnPage('/admin/dash');

//see things on the landing page
$I->see('Add modules');
$I->see('Add an item');
$I->see('My Modules');
$I->see('All Items');

//Going on to the add item page
$I->click('Add an item');
$I->amOnPage('/admin/items/create');
$I->see('Add new Item');
$I->see('Item Text:');
$I->see('Default Item');
$I->see('Create Item');
$I->fillField('testing 123');
$I->click('Create Item');
$I->amOnPage('/admin/items');
$I->see('testing 123');
