<?php
$I = new FunctionalTester($scenario);
$I->am('a God Admin');
$I->wantTo('see the list of items');

// Log in as user
// When
Auth::loginUsingId(11);
$I->seeAuthentication();

// Then Create some courses...
$I->haveRecord('items', [
    'text' => 'a test item',
    'default' => 1,
]);
$I->haveRecord('items', [
    'text' => 'a second test item',
    'default' => 0,
]);
// Then
$I->seeRecord('items', ['text' => 'a test item']);
$I->amOnPage('/admin/items');
$I->see('Items', 'h1');
$I->see('a test item');
$I->see('a second test item');