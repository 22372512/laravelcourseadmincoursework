<?php
$I = new FunctionalTester($scenario);
$I->am('a God Admin');
$I->wantTo('Checking the delete confirm');

//Check the log in
Auth::loginUsingID(11);
$I->seeAuthentication();

//Checking the the page

$I->amOnPage('/admin/items');
$I->see('Items');

//Trying the confirm delete.
$I->see('Error numquam esse enim et et.');
$I->see('Vero labore sunt ipsam nihil deserunt qui.');
$I->click('Delete this Item');
$I->see('Do you wish to delete?');
//$I->click('OK', 'input[type="confirm"]');
$I->dontSee('Error numquam esse enim et et.');
$I->see('Vero labore sunt ipsam nihil deserunt qui.');
