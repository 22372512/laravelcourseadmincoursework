<?php
$I = new FunctionalTester($scenario);
$I->am('A moduleleader1');
$I->wantTo('Check that I can add an item');

//Auththenication
Auth::loginUsingId(13);
$I->seeAuthentication();

//See landing page

$I->amOnPage('/admin/dash');

//see things on the landing page
$I->see('Add modules');
$I->see('Add an item');
$I->see('My Modules');
$I->see('All Items');

//Going to add a new item
$I->click('Add an item');
$I->see('Item Text');
$I->fillField('text', 'Testing');
$I->click('Create Item');
$I->see('Testing');

