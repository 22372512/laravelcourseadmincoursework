<?php 
$I = new FunctionalTester($scenario);
$I->am('a notadmin');
$I->wantTo('Log in to see the not admins page.');

//Log in as the module leader
Auth::loginUsingID(12);
$I->seeAuthentication();


//Go to the module leader page?
$I->amOnPage('/admin/dash');

//What to expect on that page
$I->see('My courses');
$I->see('My Modules');
$I->see('My Items');
