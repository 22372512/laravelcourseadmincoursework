<?php
$I = new FunctionalTester($scenario);
$I->am('A moduleleader1');
$I->wantTo('Check that I can Login');

//Auththenication
Auth::loginUsingId(13);
$I->seeAuthentication();

//See landing page

$I->amOnPage('/admin/dash');

//see things on the landing page
$I->see('Add modules');
$I->see('Add an item');
$I->see('My Modules');
$I->see('All Items');