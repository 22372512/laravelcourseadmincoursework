<?php
$I = new FunctionalTester($scenario);
$I->am('A moduleleader1');
$I->wantTo('Check that I can use the mark as button');

//Auththenication
Auth::loginUsingId(13);
$I->seeAuthentication();

//See landing page

$I->amOnPage('/admin/dash');

//see things on the landing page
$I->see('Add modules');
$I->see('Add an item');
$I->see('My Modules');
$I->see('All Items');

//Going on to module page
$I->click('My Modules');
$I->amOnPage('/admin/modules');
$I->see('testing one');
$I->click('testing one');
$I->amOnPage('/admin/modules/11');
$I->see('testing one');
$I->see('Add/Remove Item');
//Clicking on complete button
$I->see('Accusamus et libero minima delectus illum ut.');
$I->seeElement('input', ['value'  => '5'], ['name' => '5markuncomplete']);
$I->see('Accusantium est quis omnis aut in.');
$I->seeElement('input', ['value'  => '19'], ['name' => '19markcomplete']);
$I->click('5markuncomplete');
$I->seeElement('input', ['value'  => '5'], ['name' => '5markcomplete']);
$I->seeElement('input', ['value'  => '19'], ['name' => '19markcomplete']);

