CREATE DATABASE  IF NOT EXISTS `courseadminfinal` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `courseadminfinal`;
-- MySQL dump 10.13  Distrib 5.6.27, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: courseadminfinal
-- ------------------------------------------------------
-- Server version	5.6.27-0ubuntu0.15.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `course_module`
--

DROP TABLE IF EXISTS `course_module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course_module` (
  `course_id` int(10) unsigned NOT NULL,
  `module_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`module_id`,`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course_module`
--

LOCK TABLES `course_module` WRITE;
/*!40000 ALTER TABLE `course_module` DISABLE KEYS */;
INSERT INTO `course_module` VALUES (3,1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,2,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,2,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,3,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,3,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(1,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,5,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,6,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,6,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(1,7,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,7,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(1,8,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,8,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(1,9,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,9,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,9,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(1,10,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,10,'0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `course_module` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `courses`
--

DROP TABLE IF EXISTS `courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `leader` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `courses_code_unique` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courses`
--

LOCK TABLES `courses` WRITE;
/*!40000 ALTER TABLE `courses` DISABLE KEYS */;
INSERT INTO `courses` VALUES (1,'Computing','BIS52239631',5,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,'Software & Systems','BIS26630650',7,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,'Web','BIS85084598',8,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,'Application Development','BIS41862460',5,'0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `courses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_module`
--

DROP TABLE IF EXISTS `item_module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_module` (
  `item_id` int(10) unsigned NOT NULL,
  `module_id` int(10) unsigned NOT NULL,
  `complete` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`module_id`,`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_module`
--

LOCK TABLES `item_module` WRITE;
/*!40000 ALTER TABLE `item_module` DISABLE KEYS */;
INSERT INTO `item_module` VALUES (1,1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(13,1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(19,1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(24,1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(26,1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(28,1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(29,1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(30,1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(33,1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(37,1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(38,1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(1,2,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,2,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,2,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,2,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,2,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(22,2,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(1,3,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,3,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,3,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,3,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,3,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(6,3,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(22,3,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(1,4,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,4,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,4,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,4,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,4,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(15,4,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(23,4,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(24,4,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(1,5,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,5,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,5,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,5,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,5,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(7,5,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(10,5,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(35,5,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(1,6,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,6,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,6,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,6,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,6,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(11,6,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(14,6,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(23,6,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(25,6,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(35,6,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(1,7,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,7,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,7,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,7,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,7,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(9,7,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(10,7,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(13,7,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(15,7,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(22,7,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(39,7,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(40,7,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(1,8,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,8,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,8,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,8,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,8,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(6,8,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(9,8,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(15,8,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(23,8,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(24,8,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(30,8,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(34,8,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(37,8,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(1,9,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,9,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,9,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,9,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,9,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(15,9,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(1,10,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,10,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,10,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,10,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,10,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(14,10,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(26,10,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(27,10,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(28,10,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(34,10,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(35,10,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(39,10,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(1,11,0,'0000-00-00 00:00:00','2015-11-27 14:20:10'),(2,11,0,'0000-00-00 00:00:00','2015-11-27 14:08:46'),(3,11,0,'0000-00-00 00:00:00','2015-11-25 16:24:34'),(4,11,1,'0000-00-00 00:00:00','2015-11-27 14:36:15'),(5,11,1,'0000-00-00 00:00:00','2015-11-27 15:28:53'),(13,11,1,'0000-00-00 00:00:00','2015-11-27 14:55:55'),(19,11,1,'0000-00-00 00:00:00','2015-11-27 14:52:41'),(26,11,1,'0000-00-00 00:00:00','2015-11-27 14:55:18'),(1,12,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,12,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,12,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,12,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,12,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(22,12,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(34,12,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(35,12,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(1,13,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,13,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,13,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,13,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,13,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(8,13,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(10,13,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(17,13,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(23,13,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(33,13,0,'0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `item_module` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `default` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items`
--

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` VALUES (1,'Praesentium deserunt ipsam est.',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,'Et aliquid cumque assumenda facilis et reiciendis voluptas.',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,'Vel quia ipsam accusantium.',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,'Enim quidem quo voluptatem magni mollitia.',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,'Accusamus et libero minima delectus illum ut.',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(6,'Est sed reprehenderit exercitationem reprehenderit aut.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(7,'Sed distinctio quia sit nemo laboriosam.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(8,'Harum voluptas sit est omnis est.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(9,'Soluta nihil soluta a omnis.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(10,'Fugiat voluptatem quibusdam aspernatur veritatis cumque aperiam consequatur.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(11,'Eum nulla qui labore debitis.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(12,'Ratione at saepe et magnam.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(13,'Deleniti a praesentium ut.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(14,'Repellat earum cupiditate odio autem laborum quia.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(15,'Eius non veniam at id iure ipsa totam.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(16,'Totam aperiam et est.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(17,'Facilis laborum autem similique nulla repudiandae corrupti.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(18,'Omnis facilis et ut aut culpa facere rerum.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(19,'Accusantium est quis omnis aut in.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(20,'Omnis pariatur molestiae maxime aliquam quaerat.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(21,'Maxime ipsum et nesciunt consequatur rerum.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(22,'Ut aut quam porro explicabo cupiditate expedita sed.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(23,'Facere et voluptatem doloribus quaerat quia qui autem error.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(24,'Et perferendis occaecati vitae labore officiis cupiditate id.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(25,'Deserunt omnis voluptatibus ea deserunt numquam sunt perspiciatis.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(26,'Autem est voluptatibus labore earum eaque.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(27,'Occaecati sint mollitia iste vel quisquam inventore.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(28,'Est adipisci exercitationem occaecati.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(29,'Et enim et est natus est fugiat quisquam.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(30,'Magnam quas ut quo id aut.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(31,'Sed eum in qui eos ut.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(32,'Sequi officia itaque maiores.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(33,'Illo dignissimos et fuga.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(34,'Ut omnis tenetur quae autem.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(35,'Voluptatem dicta voluptas placeat qui nostrum doloribus reprehenderit.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(36,'Ullam excepturi et ut et.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(37,'Itaque voluptate quis blanditiis aut reprehenderit et.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(38,'Officiis qui id quia possimus quasi in aut aut.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(39,'Porro impedit non velit non.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(40,'Earum ipsum eveniet repellendus voluptatem officia.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1),('2015_07_21_110723_create_courses_table',1),('2015_07_21_110733_create_modules_table',1),('2015_07_21_110813_create_items_table',1),('2015_07_21_110847_create_itemmodule_table',1),('2015_07_21_110855_create_coursemodule_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modules`
--

DROP TABLE IF EXISTS `modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `leader` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `modules_code_unique` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modules`
--

LOCK TABLES `modules` WRITE;
/*!40000 ALTER TABLE `modules` DISABLE KEYS */;
INSERT INTO `modules` VALUES (1,'Module 1','CIS4680',9,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,'Module 2','CIS3432',9,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,'Module 3','CIS3240',3,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,'Module 4','CIS3873',3,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,'Module 5','CIS1518',6,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(6,'Module 6','CIS3681',7,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(7,'Module 7','CIS3453',9,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(8,'Module 8','CIS1228',4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(9,'Module 9','CIS2173',2,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(10,'Module 10','CIS2815',6,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(11,'testing one','1234',13,'2015-11-25 15:18:22','2015-11-25 15:18:22'),(12,'testing two','123456',13,'2015-11-25 15:18:38','2015-11-25 15:18:38'),(13,'testing three','123456778',13,'2015-11-25 15:19:02','2015-11-25 15:19:02');
/*!40000 ALTER TABLE `modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `admin` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Gunnar Abernathy','sHudson@gmail.com','lA9n2INvLa','sahfGqviqv','2015-11-25 12:12:42','2015-11-25 12:12:42',0),(2,'Aurore Homenick','Diamond45@hotmail.com','NFzrIiFiig','ArhW5C5AK3','2015-11-25 12:12:42','2015-11-25 12:12:42',0),(3,'Devan Conn','kCollier@gmail.com','tmtYlU2QcG','exy0ikiNry','2015-11-25 12:12:42','2015-11-25 12:12:42',0),(4,'Damian Johnson V','Koepp.Mae@Tromp.net','0JB3tI7o7s','Ekha0Wte89','2015-11-25 12:12:42','2015-11-25 12:12:42',0),(5,'Davonte King','yHoeger@Schneider.com','iuDMSR5uFo','AjvqKz1iyt','2015-11-25 12:12:43','2015-11-25 12:12:43',0),(6,'Dr. Daniella Miller DVM','Adrianna07@Hartmann.com','awfh0sMsyc','FwUNcdhKTn','2015-11-25 12:12:43','2015-11-25 12:12:43',0),(7,'Geraldine Schmidt','Schmitt.Brionna@Kub.info','Cm1yezMTbb','EvW6dRUiMo','2015-11-25 12:12:43','2015-11-25 12:12:43',0),(8,'Aidan Pagac','Weldon97@yahoo.com','T02XFCmCFK','17Ggoi2nnc','2015-11-25 12:12:43','2015-11-25 12:12:43',0),(9,'Floyd Gusikowski','Sean.Littel@Marks.org','AnE4DEPVSO','2GcAS2pQSZ','2015-11-25 12:12:44','2015-11-25 12:12:44',0),(10,'Malvina Haley','Wiegand.Norwood@Keebler.com','CS7gwfHH4p','CpbGSnxbbM','2015-11-25 12:12:44','2015-11-25 12:12:44',0),(11,'admin','admin@example.com','$2y$10$3RfAluVo3QcgPMSKMMP5iuUoIFOoW8RoedKwxjG8wXw5a568E4Dsi',NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00',1),(12,'notadmin','notadmin@example.com','$2y$10$4e5m3M7OnPs601sg3wzvEOx/BPoBDH3P/W6FDdy18X13uHPhAWUYu',NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00',0),(13,'moduleleader1','ml@example.com','$2y$10$rqR0RU8WmsJaKSAr1M.FIOeUT6Ai6rajKgZHbmWrfTrXyjQ4AyWWe',NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00',2);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-11-27 15:31:09
