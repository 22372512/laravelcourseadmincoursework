<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // clear the table of data.
        DB::table('users')->truncate();

        // use a factory method to create 10 random users.
        factory('App\User', 10)->create();

        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@example.com',
            'password' => bcrypt('password'),
            'admin' => '1'
        ]);

        DB::table('users')->insert([
            'name' => 'notadmin',
            'email' => 'notadmin@example.com',
            'password' => bcrypt('password'),
            'admin' => '0'
        ]);
         //This is the information for the module leaders account
        DB::table('users')->insert([
            'name' => 'moduleleader1',
            'email' => 'ml@example.com',
            'password' => bcrypt('password'),
            'admin' => '2'
        ]);
    }
}
