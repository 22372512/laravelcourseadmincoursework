<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Module extends Model
{
    protected $table = 'modules';
    protected $fillable = ['title', 'code', 'leader'];

    public function courses()
    {
        return $this->belongsToMany('App\Course')->withTimestamps();
    }

    public function items()
    {
        return $this->belongsToMany('App\Item')->withPivot('complete')->withTimestamps()->orderBy('text', 'asc');
    }

    public static function itemModuleAssociations($id)
    {
        // set global variables
        $GLOBALS['module_id'] = $id;
        // create a join on multiple columns that also creates a new column 'checked'. This join can not be done in
        // eloquent as we need it to return all items, not just those which are associated as per the above items method.

        // first line returns all items
        // Second part left joins to the item_module table only for the specific module
        // this selects then gets all of the item data and adds an additional column' checked' making the value 1 if the association exists and 0 if it does not.
        // order the items by default and then by alpha.
        // the 'get' then simply runs the query.
        return DB::table('items')
            ->leftJoin('item_module', function($join)
            {
                $join->on('item_module.item_id', '=', 'items.id')
                    ->on('item_module.module_id', '=', DB::Raw($GLOBALS['module_id']));
            })
            ->select('items.*', DB::Raw('IF(`item_module`.`module_id` IS NULL, 0, 1) AS `checked`'))
            ->orderBy('items.default', 'desc')
            ->orderBy('text', 'asc')
            ->get();
    }
}
