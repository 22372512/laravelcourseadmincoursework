<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Authentication routes...
Route::get('/', 'Auth\AuthController@getLogin');
Route::get('admin/login', 'Auth\AuthController@getLogin');
Route::post('admin/login', 'Auth\AuthController@postLogin');
Route::get('admin/logout', 'Auth\AuthController@getLogout');


// secure admin pages in group.
Route::group(['middleware' => 'auth'],
function()
{
    Route::get('admin/dash', 'DashController@index');
    Route::resource('admin/courses', 'CoursesController');
    Route::resource('admin/modules', 'ModulesController');
    Route::resource('admin/modules/', 'ModulesController');
    Route::resource('admin/items', 'ItemsController');
    Route::resource('admin/itemmodule', 'ItemModuleController');
}
);

