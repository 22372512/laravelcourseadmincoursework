@extends('layouts.master')
@section('title')
    Dashboard
@stop
@section('content')

    @if ( Session::get('message'))
        <div class="alert-box warning large-12 columns">
            {{ Session::get('message') }}
        </div>
    @endif

    <h1>Dashboard</h1>
    <div class="large-12 columns">
        <p>
            <?php
                if (Auth::user())
                { echo "you are logged in as ";
                    echo Auth::user()->name;
                }
                ?>
        </p>
            <?php $adminstatus = (Auth::user()->admin); ?>
            @if ($adminstatus == '1')
                <div class="row left">
                        <!-- return a link to add new course -->
                    <a href="{{ url('admin/courses/create') }}" class="button">
                        Add a new course
                    </a>
                    <a href="{{ url('admin/modules/create') }}" class="button">
                        Add a new module
                    </a>
                    <a href="{{ url('admin/items/create') }}" class="button">
                        Add a new item
                    </a>
                </div>
                <div class="row left">
                    <a href="{{ url('admin/courses') }} " class="button">
                        All Courses
                    </a>
                    <a href="{{ url('admin/modules') }} " class="button">
                        All Modules
                    </a>
                    <a href="{{ url('admin/items') }} " class="button">
                        All Items
                    </a>
                </div>
                <!--Module leader landing page-->
             @elseif ($adminstatus == '2') 
                <div class="row left">
                <!--ADD NEW COURSE AND ITEMS-->
                    <a href="{{ url('admin/modules/create') }}" class="button">
                        Add modules
                    </a>
                    <a href="{{ url('admin/items/create') }}" class="button">
                        Add an item
                    </a>
                    <!--SEE CURENT MODULES AND ITEMS-->
                    <a href="{{ url('admin/modules') }}" class="button">
                        My Modules
                    </a>
                    <a href="{{ url('admin/items') }}" class="button">
                        All Items
                    </a>

                </div>  
            @else ($adminstatus == '0')
                    <!-- else return a link to show all this persons courses -->
                <div class="row left">
                    <a href="{{ url('admin/courses') }} " class="button">
                        My Courses
                    </a>
                    <a href="{{ url('admin/modules') }} " class="button">
                        My Modules
                    </a>
                    <a href="{{ url('admin/items') }} " class="button">
                        My Items
                    </a>
                </div>
                
           
            @endif

    </div>
@stop