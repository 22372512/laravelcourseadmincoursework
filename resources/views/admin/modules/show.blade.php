@extends('layouts.master')
@section('title')
    Module - {{ $module->code }} - {{ $module->title }}
@stop
@section('content')

    @if ( Session::get('message'))
        <div class="alert-box warning large-12 columns">
            {{ Session::get('message') }}
        </div>
    @endif

    <h1 class="small-12 columns">{{ $module->title }}</h1>
    <div class="small-12 columns">
        <p> <strong>Module code:</strong> {{ $module->code }}</p>
        <p> <strong>Module Leader:</strong> {{ $moduleleader->name }}</p>


<!-- Here shows the total percentage of completion of this module-->
        <?php
        //Gets the total of this module
        $total = $module->items()->count();
        //counts how many complered items there are
        $completion = $completed->where('module_id', $module->id)->count();
        //calculates the percentage
        $percent = (($completion / $total) * 100);

        ?>
        <!--Shows the percentage in a bar and as a number -->
        <div class="progress">
            <span class="meter" style="width:{{ $percent }}%"></span>
            <p>{{ $percent }}% Completed</p>
        </div>


        <br/>
         <!--Link to the add and remove items page -->
        <a href="{{ route('admin.modules.edit', $module->id) }}" class="button" name="ahref1">Add/Remove Item</a>
        <div>
            <h2>Associated Items</h2>
            @if ( !$module->items->count() )
                Your Module has no Items linked.
            @else
                <ul class="no-bullet small-12 columns">
                    @foreach( $module->items as $item )
                        <li>
                            <p class="small-8 row columns">{{ $item->text }}</p>
                            <!-- <form method="PUT" action="{{url('/admin/itemmodule') }}" enctype="multipart/form-data" id="form" data-abide> -->
                            {!! Form::model($item, array('method' => 'put', 'route' => ['admin.itemmodule.update', $item->id], 'data-abide' => '')) !!}


                            <input type="hidden" name="item_id" value="{{ $item->id }}" />
                            <input type="hidden" name="module_id" value="{{ $module->id }}" />
                            <!-- switch the buttons based on state of item -->
                            @if ($item->pivot->complete != '1')
                                    <!-- completed submit button -->
                            <div class="small-4 columns">
                                <button type="submit" name="{{$item->id}}markcomplete" class="button right tiny">Mark as Complete</button>
                            </div>
                            <input type="hidden" name="complete" value="1" />
                            @else
                                    <!-- uncompleted submit button -->
                            <div class="small-4 columns">
                                <button type="submit" name="{{$item->id}}markuncomplete" class="button right tiny">Mark as Un-Complete</button>
                            </div>

                            <input type="hidden" name="complete" value="0" />


                            @endif

                            {!! csrf_field() !!}

                        </li>
                    @endforeach
                </ul>
            @endif
        </div>


        <a href="{{ route('admin.modules.edit', $module->id) }}" class="button small warning right" name="ahref2">Edit Module</a>
    </div>
@stop
