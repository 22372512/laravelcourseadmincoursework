<!--Add Items functionality-->
<!--Extending from the main page which has footer/header-->
@extends('layouts.master')
        <!--Adds a title to the page-->
@section('title')
    Add Items To {{ $module->title }}
    @stop
            <!--Content starts here-->
    @section('content')


            <!--Shows the title with adding items to that title-->
    <h1 class="small-12 columns">Add items to {{ $module->title }}</h1>

    @if (count($errors) > 0)
        <div class="alert-box alert small-12 columns">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

                <!--Openning the form-->
        {!! Form::model($model, array('method' => 'put', 'route' => ['admin.modules.update', $module->id], 'data-abide' => '')) !!}


                <!--Shows the current items-->
        <!--Makes sure there are some items, if no items if statement wont run-->
        @if (count($items)>0)
                <!--If true then it will show all the items-->
        @include('admin.modules.itemModuleAssociations')

        @endif

                <!--Final submitting button-->

        {!! Form::submit( 'Add Items', array('class'=>'button left')) !!}
        {!! csrf_field() !!}
        {!! Form::close() !!}


@stop
