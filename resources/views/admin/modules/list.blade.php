@extends('layouts.master')
@section('title')
    All Modules
@stop
@section('content')
    <?php $adminstatus = (Auth::user()->admin) ?>
    @if ($adminstatus == '1')
        <h1>Modules <a href="{{ url('admin/modules/create') }}" class="button right success small">
                Add a new module
            </a></h1>
    @else
        <h1>Modules</h1>
        @endif

        @if(Auth::check())
                <!--  The user is logged in... -->
        <div class="row small-12 columns">

            @if ($adminstatus == '1')
                    <!-- return a list of all courses -->
            <div class="small-12 columns">
                @foreach($allModules as $aModule)
                    <div class="row">
                        {!! Form::open(array('url' => '/admin/modules/' . $aModule->id)) !!}
                        <a href="{{ route('admin.modules.show', [$aModule->id]) }}" class="item">{{ $aModule->title }}</a>
                        {!! Form::hidden('_method', 'DELETE') !!}
                        {!! Form::submit('Delete this Module', array('class' => 'button tiny alert right', 'id' => $aModule->title . ' delete', 'name' => $aModule->title . ' delete')) !!}
                        {!! Form::close() !!}
                        <hr />
                    </div>
                @endforeach

            </div>
            @else

                    <!-- return only courses the staff are course leaders of and if none then return a message saying none to show. -->
            <?php $myModules = $allModules->where('leader', (Auth::user()->id)) ?>
            <ul class="no-bullet">
                @foreach ($myModules as $myModule)
                    <li>
                        <br/>
                        <a href="{{ route('admin.modules.show', [$myModule->id]) }}" class="item">{{ $myModule->title }}</a>
                         <!--This is the percent function that shows the percentage of all the modules
Has 3 variables to calculate the percentage
Then a progress bar using foundation to display it
With the number beneath it -->      
                        <div class="progress">

                            <?php
                             //Counts the total of all the items in a module
                            $total = $myModule->items()->count();
                           //this counts the completed items of the module
                            $completion = $completed->where('module_id', $myModule->id)->count();
                            //calculates the percentage by completed divided by total times 100
                            $percent = (($completion / $total) * 100);

                            ?>
                             <!--Inputs percentage into a bar and shows the percent completed-->
                            <span class="meter" style="width:{{ $percent }}%"></span>
                            <p>{{ $percent }}% Completed</p>



                        </div>
                    </li>
                    <br/>
                @endforeach
            </ul>

            @endif
        </div>

    @endif



@stop
