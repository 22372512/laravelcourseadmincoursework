@extends('layouts.master')
@section('title')
    All Items
@stop
@section('content')
<script>
   function Deleting()
   {
    var dconfirm = confirm("Do you wish to delete?");
    if (dconfirm)
            return true;
        else 
            return false;
   }
</script>
    @if ( Session::get('message'))
        <div class="alert-box warning large-12 columns">
            {{ Session::get('message') }}
        </div>
    @endif
    <h1 class="small-12 columns">Items <a href="{{ url('admin/items/create') }}" class="button small right success">
            Add a new item
        </a></h1>

    @if(Auth::check())
            <!--  The user is logged in... -->
    <div class="row small-12 columns">

        <!-- return a list of all courses -->
        <div class="small-12 columns">
            @foreach($allItems as $item)
                <div class="row">
                    {!! Form::open(array('url' => '/admin/items/' . $item->id, 'onsubmit' => 'return Deleting()')) !!}
                    <p class="item small-8 columns">{{ $item->text }}</p>
                    <a href="{{ route('admin.items.edit', $item->id) }}" class="button tiny warning " name="{{ $item->id }}">Edit Item</a>
                    {!! Form::hidden('_method', 'DELETE') !!}
                    {!! Form::submit('Delete this Item', array('class' => 'button tiny alert right', 'name' =>  'delete' . $item->id)) !!}
                    {!! Form::close() !!}

                    <hr />
                </div>

            @endforeach
        </div>

    </div>

    @endif



@stop